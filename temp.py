'''
class Panel1(wx.Panel):
	"""class Panel1 creates a panel with an image on it, inherits wx.Panel"""
	def __init__(self, parent, id):
		# create the panel
		wx.Panel.__init__(self, parent, id)
		try:
			# pick an image file you have in the working folder
			# you can load .jpg  .png  .bmp  or .gif files
			image_file = 'flag.png'
			bmp1 = wx.Image(image_file, wx.BITMAP_TYPE_ANY).ConvertToBitmap()
			# image's upper left corner anchors at panel coordinates (0, 0)
			self.bitmap1 = wx.StaticBitmap(self, -1, bmp1, (0, 0))
			# show some image details
			str1 = "%s  %dx%d" % ('here', bmp1.GetWidth(), bmp1.GetHeight())
			parent.SetTitle(str1)
		except IOError:
			print "Image file %s not found"
			raise SystemExit

		# button goes on the image --> self.bitmap1 is the parent
		#self.button1 = wx.Button(self.bitmap1, id=-1, label='Button1', pos=(8, 8))
app = wx.PySimpleApp()

# create a window/frame, no parent, -1 is default ID
# change the size of the frame to fit the backgound images
frame1 = wx.Frame(None, -1, "An image on a panel", size=(1046, 669))
# create the class instance
panel1 = Panel1(frame1, -1)

frame1.Show(True)
app.MainLoop()
'''

import wx

class HomePage(wx.Frame):

	def __init__(self, parent, title):
		image_file = 'flag.png'
		bmp1 = wx.Image(image_file, wx.BITMAP_TYPE_ANY).ConvertToBitmap()
		width = bmp1.GetWidth()
		height = bmp1.GetHeight()

		super(HomePage, self).__init__(parent, title='Alert System Team 2', size=(width, height), style = wx.MINIMIZE_BOX | wx.SYSTEM_MENU | wx.CAPTION | wx.CLOSE_BOX)

		# image's upper left corner anchors at panel coordinates (0, 0)
		self.bitmap1 = wx.StaticBitmap(self, -1, bmp1, (0, 0))

		# two buttons
		self.textBtn = wx.Button(self.bitmap1, label="Text Message",  pos=(700, 60))
		self.textBtn.Bind(wx.EVT_BUTTON, lambda event: self.textMessage(width, height))

		self.alertBtn = wx.Button(self.bitmap1, label="Alert Samples",  pos=(700, 160))
		self.alertBtn.Bind(wx.EVT_BUTTON, lambda event: self.playAlert())

		# show some image details in title
		#str1 = "%s  %dx%d" % ('Alert System Team 2', width, height)
		self.Centre()
		self.Show()

	def textMessage(self, width, height):
		self.Hide()

		frame = wx.Frame(None, -1, title=('Alert System Team 2/Text Message'), size=(width/2, height/2), style = wx.MINIMIZE_BOX | wx.SYSTEM_MENU | wx.CAPTION | wx.CLOSE_BOX)
		loadBtn = wx.Button(frame, -1, label='Load phone number file', pos=(200, 60))
		sendBtn = wx.Button(frame, -1, label='Send', pos=(200, 80))

		backBtn = wx.Button(frame, -1, label='< Back', pos=(200, 100))
		backBtn.Bind(wx.EVT_BUTTON, lambda event: self.backToMainPage(frame))

		#resultBtn.Bind(wx.EVT_BUTTON, lambda event: self.interact.seeResult(self.page3, self.resultPage, 1))
		#frame.backBtn.Bind(wx.EVT_BUTTON, lambda event: self.playAlert())

		frame.Centre()
		frame.Show()
		frame.Bind(wx.EVT_CLOSE, self.Destroy()) # exit python when X is clicked

	def playAlert(self):
		self.Hide()

	def backToMainPage(self, frame):
		self.Hide()
		frame.Hide()

if __name__ == '__main__':

	app = wx.App()
	HomePage(None, title='')
	app.MainLoop()
